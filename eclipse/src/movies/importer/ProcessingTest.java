/*Sirine Aoudj, 1935903*/
package movies.importer;

import java.io.IOException;

public class ProcessingTest {

	public static void main(String[] args)throws IOException {
		
		String source = "C:\\Users\\admin\\Desktop\\test_lab5";
		String output = "C:\\Users\\admin\\Desktop\\output_lab5";
		LowercaseProcessor test = new LowercaseProcessor(source,output);
		test.execute();
		
		String source1 = "C:\\Users\\admin\\Desktop\\output_lab5";
		String output1 = "C:\\Users\\admin\\Desktop\\output2";
		RemoveDuplicates test2 = new RemoveDuplicates(source1,output1);
		test2.execute();
	}

}
