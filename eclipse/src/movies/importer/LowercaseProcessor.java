/*Sirine Aoudj, 1935903*/
package movies.importer;

import java.util.*;

public class LowercaseProcessor extends Processor{

	public LowercaseProcessor(String source, String output) {
		super(source, output, true);
	}

	public ArrayList<String> process(ArrayList<String> list){
		ArrayList<String> asLower = new ArrayList<String>();
		for(int i = 0; i < list.size(); i++) {
			asLower.add(list.get(i).toLowerCase());
		}
		return asLower;
	}
}
